import { Component, OnInit } from '@angular/core';
import { Pedido } from '../models/pedido';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-pedido',
  templateUrl: './pedido.component.html',
  styleUrls: ['./pedido.component.scss']
})
export class PedidoComponent implements OnInit {

  pedido: Pedido =  new Pedido();

  constructor() { }

  ngOnInit(): void {
  }

  agregarProducto(){
    this.pedido.pedidoDetalle.push({
      cantidad: 20,
      precio: 15,
      producto: 'Agua',
      total: 300
    });
    Swal.fire({
      icon: 'success',
      title: 'Producto agregado',
      text: 'Se agregó correctamente'
    });
  }

  elHijoEliminoUnProducto(event){
    this.pedido.pedidoDetalle.splice(event.id, 1);
    console.log(event.id);
    Swal.fire({
      title: 'Producto eliminado',
      text: 'Se eliminó correctamente',
      icon: 'warning'
    });
  }
}
