import { Component, ElementRef, ViewChild } from '@angular/core';
import { HijoComponent } from './hijo/hijo.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ejemploCore';
  @ViewChild('inputNombre', {static: true}) inputNombre: ElementRef;

  @ViewChild(HijoComponent, {static: true}) hijo: HijoComponent;

  constructor(){
  }

  mostrar(){
    console.log(this.inputNombre.nativeElement.value);
    this.inputNombre.nativeElement.style.background = "yellow";

    this.hijo.titulo = 'Lo modifique desde el padre';
  }
}
