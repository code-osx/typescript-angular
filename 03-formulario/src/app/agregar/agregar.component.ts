import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

interface Usuario{
  nombre:string,
  correo:string,
  pass:string
}

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.scss']
})
export class AgregarComponent implements OnInit {
  formularioCreado: FormGroup;
  esNuevo:boolean = true;
  usuarios:Array<Usuario> = new Array<Usuario>();
  posicionEditar:number = -1;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.crearFormulario();
  }

  crearFormulario(){
    this.formularioCreado = this.formBuilder.group({
      nombre: ['', Validators.required],
      correo: ['@correo.com', Validators.compose([
        Validators.required, 
        Validators.email
      ])],
      pass: ['', Validators.compose([
        Validators.required,
        Validators.minLength(6)
      ])]
    });
  }

  agregar(){
    this.usuarios.push(this.formularioCreado.value as Usuario);
    this.formularioCreado.reset();
  }

  editarUsuario(posicion:number){
    this.formularioCreado.setValue({
      nombre: this.usuarios[posicion].nombre,
      correo: this.usuarios[posicion].correo,
      pass: this.usuarios[posicion].pass
    });

    this.posicionEditar = posicion;
    this.esNuevo = false;
  }

  editar(){
    this.usuarios[this.posicionEditar].nombre = this.formularioCreado.value.nombre;
    this.usuarios[this.posicionEditar].correo = this.formularioCreado.value.correo;
    this.usuarios[this.posicionEditar].pass = this.formularioCreado.value.pass;

    this.formularioCreado.reset();
    this.esNuevo = true;
    this.posicionEditar = -1;
  }

  eliminarUsuario(posicion:number){
    this.usuarios.splice(posicion, 1);
  }
}
