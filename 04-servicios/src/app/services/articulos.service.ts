import { Injectable } from '@angular/core';
import { Articulo } from '../models/articulo';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class ArticulosService {
  articulo: Articulo = new Articulo();
  constructor(private http: HttpClient) {

  }


  leerNoticias(): Observable<Articulo[]>{
    return this.http.get<Array<Articulo>>('https://jsonplaceholder.typicode.com/posts');
  }

  leerUsuario(id: number): Observable<User>{
    return this.http.get<User>('https://jsonplaceholder.typicode.com/users/'+id);
  }

  leerTodosLosUsuarios(): Observable<User[]>{
    return this.http.get<User[]>('https://jsonplaceholder.typicode.com/users');
  }

  guardarArticulo(articulo: Articulo): Observable<Articulo>{
    return this.http.post<Articulo>('https://jsonplaceholder.typicode.com/posts', articulo);
  }

  borrarArticulo(id: number): Observable<any>{
    return this.http.delete<any>('https://jsonplaceholder.typicode.com/posts/'+id);
  }

  actualizarArticulo(articulo: Articulo): Observable<Articulo>{
    return this.http.put<Articulo>(`https://jsonplaceholder.typicode.com/posts/${articulo.id}`, articulo);
  }
}