import { Component, OnInit } from '@angular/core';
import { Articulo } from '../models/articulo';
import { UsuarioService } from '../services/usuario.service';
import { ArticulosService } from '../services/articulos.service';
import { Router } from '@angular/router';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  articulos: Array<Articulo> = new Array<Articulo>();

  constructor(
    private usuarioInyectado: UsuarioService, 
    private articuloInyectado: ArticulosService,
    private Ruta: Router) { }

  ngOnInit(): void {
    this.articuloInyectado.leerNoticias().subscribe((articulosDesdeAPI)=>{
      this.articulos = articulosDesdeAPI;
    });
  }

  irAlDetalle(articulo: Articulo){
    this.articuloInyectado.articulo = articulo;
    this.Ruta.navigateByUrl('/articulo-detalle');
  }

  borrar(id:number){
    this.articuloInyectado.borrarArticulo(id).subscribe((datos)=>{
      console.log(datos);
      console.log("Articulo borrado");
    });
  }

  actualizar(articulo: Articulo){
    //Cambios manualmente
    articulo.title= "Nuevo titulo";
    articulo.body = "Nueva informacion";
    articulo.userId = 12;

    this.articuloInyectado.actualizarArticulo(articulo).subscribe((articuloRecibido)=>{
      console.log(articuloRecibido);
    });
  }

}
