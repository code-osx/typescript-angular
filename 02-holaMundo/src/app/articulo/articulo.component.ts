import { Component, OnInit } from '@angular/core';
import { Articulo } from '../models/articulo';
import { Router } from '@angular/router';

@Component({
  selector: 'app-articulo',
  templateUrl: './articulo.component.html',
  styleUrls: ['./articulo.component.scss']
})
export class ArticuloComponent implements OnInit {
  arts: Array<Articulo> =  new Array<Articulo>();

  constructor(private ruta: Router) { }

  ngOnInit(): void {
    this.arts.push(
      {nombre: 'TV', descripcion: 'Es grande', precio:10232},
      {nombre: 'Laptop', descripcion: 'Nueva lap', precio:15232},
      {nombre: 'Celular', descripcion: 'Mucha ram', precio:6232},
      {nombre: 'Radio', descripcion: 'Es pequeña', precio:232}
    );
  }

  pasarParametro(artRecib:Articulo){
    this.ruta.navigate(['articuloDetalle', {articulo: JSON.stringify(artRecib) }]);
  }

}
