import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-titulos',
  templateUrl: './titulos.component.html',
  styleUrls: ['./titulos.component.scss']
})
export class TitulosComponent implements OnInit {
  nombre: string = "Oscar";

  alumno: any = {
    nombre: 'Oscar',
    edad: 23
  }

  imagen:string = 'https://image.freepik.com/iconos-gratis/facebook-logo-esquinas-redondeadas_318-9850.jpg';

  val_inpt: string = "Hola mundo desde una variable";

  twb:string = '';

  correo:string ='';
  password:string = '';

  name:string = '';

  url2:string = 'https://images.vexels.com/media/users/3/137418/isolated/preview/f95d43a9a4f5e0cd4a0b6e79cc99d190-icono-de-tumblr-logo-by-vexels.png';
  
  constructor() { }

  ngOnInit(): void {
  }

  ingresar(){
    console.log(this.correo);
    console.log(this.password);
  }

  mostrarAlerta(){
    alert("Has dado doble click");
  }

  cambiandoValor(){
    console.log("El valor de name esta combiando: "+this.name);
  }

  escribir(evento){
    console.log(evento);
    console.log(evento.target.value); //Valor que contiene el input
  }

  colorear(evento){
    console.log(evento);
    evento.srcElement.style.background = 'red';
  }

  mostrarEscuela(evento){
    console.log(evento);
    if(evento.key == 'Enter'){
      alert('Has presionado Enter');
    }
  }

  cambiarTam(evento){
    console.log(evento);
    evento.srcElement.style.width = "200px";
  }

  cambiarTam2(evento){
    console.log(evento);
    evento.srcElement.style.width = "400px";
  }
}
