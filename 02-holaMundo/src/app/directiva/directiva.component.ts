import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directiva',
  templateUrl: './directiva.component.html',
  styleUrls: ['./directiva.component.scss']
})
export class DirectivaComponent implements OnInit {
  cargando: boolean = true;
  nombres: Array<string> = ["Maria", "Ana", "Juan", "Pedro", "Luis"];
  pestana:string = '';
  var_cuadrado: boolean = false;

  constructor() { }

  ngOnInit(): void {
    setTimeout(() =>{
      this.cargando = false;
    }, 3000);
  }

  cambiarPestana(pestana: string){
    this.pestana = pestana;
  }

  mostrarCuadrado(){
    this.var_cuadrado = true;
  }

}
