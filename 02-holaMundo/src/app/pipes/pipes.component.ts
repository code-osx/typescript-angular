import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipes',
  templateUrl: './pipes.component.html',
  styleUrls: ['./pipes.component.scss']
})
export class PipesComponent implements OnInit {
  titulo:string = "Hola, soy un titulo";
  dinero:number = 2400;
  ganancias:number = 0.64;
  fecha: Date = new Date('05-16-97');

  constructor() { }

  ngOnInit(): void {
  }

}
