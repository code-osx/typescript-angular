function suma(n1:number, n2:number) : number {
    var total:number = n1 + n2;
    
    return total;
}

var n1:number = 5;
var n2:number = 10;
console.log('La suma de '+n1+' + '+n2+' es: '+suma(n1,n2));