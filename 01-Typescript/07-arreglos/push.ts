var nombres:string[] = ['Karen','Maria','Alex','Luna'];
console.log(nombres);

nombres.push('Carlos');
console.log(nombres);


interface Usuario{
    nombre: string;
    edad: number;
}

var usuarios: Array<Usuario> =[
    {
        nombre: 'Oscar',
        edad: 22
    },
    {
        nombre: 'Ale',
        edad: 23
    },
    {
        nombre: 'Laura',
        edad: 25
    }
];

console.log(usuarios);

usuarios.push({nombre: 'Ana', edad: 22});

console.log(usuarios);

var alan: Usuario = {
    nombre: 'Alan',
    edad: 32
};

usuarios.push(alan);

console.log(usuarios);