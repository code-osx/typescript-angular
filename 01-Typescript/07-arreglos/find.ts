interface Usuario{
    nombre: string;
    edad: number;
}

var usuarios: Usuario[] =[
    {
        nombre: 'Oscar',
        edad: 22
    },
    {
        nombre: 'Ale',
        edad: 23
    },
    {
        nombre: 'Laura',
        edad: 25
    }
];

console.log(usuarios);


var alumno_encontrado = usuarios.find((usuario)=>{
    return usuario.nombre == 'Laura';
});

console.log(alumno_encontrado);