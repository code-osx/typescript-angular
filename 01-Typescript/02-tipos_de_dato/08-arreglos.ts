var lista_nombres: Array<string>;

lista_nombres = ['Oscar', 'Juan', 'Maria', 'Ana']

console.log(lista_nombres);
console.log('El primero es: '+ lista_nombres[0]);
console.log('El segundo es: '+ lista_nombres[1]);
console.log('El tercero es: '+ lista_nombres[2]);
console.log('El ultimo es: '+ lista_nombres[3]);

var lista_numeros: Array<number> = [12,4, 6, 7];
console.log(lista_numeros);



interface Alumnos{
    nombre: string,
    edad: number
}

var lista_alumnos: Array<Alumnos> = [
    {
        nombre: 'Oscar',
        edad: 22
    },
    {
        nombre: 'Alexa',
        edad: 23
    },
    {
        nombre: 'Manuel',
        edad: 13
    }
];

console.log(lista_alumnos);