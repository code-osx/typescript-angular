interface Personas {
    nombre: string,
    apellidos: string,
    edad: number,
    peso: number
}

var persona1: Personas = {
    nombre: 'Oscar',
    apellidos: 'Sanchez',
    edad: 22,
    peso: 45,
};

console.log(persona1);