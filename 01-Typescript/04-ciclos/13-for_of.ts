interface Usuario{
    nombre: string;
    edad: number;
}

var usuarios: Array<Usuario> =[
    {
        nombre: 'Oscar',
        edad: 22
    },
    {
        nombre: 'Ale',
        edad: 23
    },
    {
        nombre: 'Laura',
        edad: 25
    }
];

console.log(usuarios);

for(let usuario of usuarios){
    console.log(usuario.nombre)
}