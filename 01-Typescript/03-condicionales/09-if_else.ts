var costo:number = 750;

if(costo >= 10000){
    let descuento = costo/100;
    descuento = descuento * 20;

    let costo_final = costo - descuento;
    console.log('Costo con descuento del 20%: ' + costo_final);
}
else if(costo > 500 && costo<10000){
    let descuento = costo/100;
    descuento = descuento * 10;

    let costo_final = costo - descuento;
    console.log('Costo con descuento del 10%: ' + costo_final);
}
else{
    console.log('No hay descuento');
}