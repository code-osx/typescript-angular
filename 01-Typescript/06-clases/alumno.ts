class Alumno{
    private nombre: string;
    private edad: number;

    constructor(){
        this.nombre = 'Usuario nuevo';
        this.edad = 0;
        console.log('Objeto Alumno creado');
    }

    getNombre() {
        return this.nombre;
    }
    setNombre(nombre: string) {
        this.nombre = nombre;
    }

    getEdad() {
        return this.edad;
    }
    setEdad(edad:number){
        this.edad = edad;
    }

    mostrarMensaje() {
        console.log('Hola '+this.nombre);
    }

    asignar(nombre:string, edad:number) {
        this.nombre = nombre;
        this.edad = edad;
    }
}

var alumno1: Alumno = new Alumno();
console.log(alumno1);
alumno1.setNombre('Alex');
alumno1.setEdad(12);

console.log(alumno1);

alumno1.mostrarMensaje();

var alumno2: Alumno = new Alumno();
console.log(alumno2);
alumno2.asignar('Alejandra', 32);
console.log(alumno2);
