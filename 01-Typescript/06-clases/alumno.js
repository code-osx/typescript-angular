var Alumno = /** @class */ (function () {
    function Alumno() {
        this.nombre = 'Usuario nuevo';
        this.edad = 0;
        console.log('Objeto Alumno creado');
    }
    Alumno.prototype.getNombre = function () {
        return this.nombre;
    };
    Alumno.prototype.setNombre = function (nombre) {
        this.nombre = nombre;
    };
    Alumno.prototype.getEdad = function () {
        return this.edad;
    };
    Alumno.prototype.setEdad = function (edad) {
        this.edad = edad;
    };
    Alumno.prototype.mostrarMensaje = function () {
        console.log('Hola ' + this.nombre);
    };
    Alumno.prototype.asignar = function (nombre, edad) {
        this.nombre = nombre;
        this.edad = edad;
    };
    return Alumno;
}());
var alumno1 = new Alumno();
console.log(alumno1);
alumno1.setNombre('Alex');
alumno1.setEdad(12);
console.log(alumno1);
alumno1.mostrarMensaje();
var alumno2 = new Alumno();
console.log(alumno2);
alumno2.asignar('Alejandra', 32);
console.log(alumno2);
