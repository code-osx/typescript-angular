//Tipos de console
console.log('Hola a todos');
console.log('Esto es desde TS');

console.clear();

console.log('Para observar los cambios del ts sin compilarlo: tsc nombre_archivo.ts --watch');
console.warn('Por favor, cerrar la puerta');
console.error('Ocurrio un error');
