import { PedidoDetalle } from './pedido-detalle';
import { Productos } from './productos';

export class Pedidos{
    idPedido: number;
    idCliente: number;
    nombreCliente: string;
    total: number;
    pedidoDetalle: Array<PedidoDetalle>;

    constructor(datos? : Pedidos){
        if(datos != null){
            this.idPedido = datos.idPedido;
            this.idCliente = datos.idCliente;
            this.nombreCliente = datos.nombreCliente;
            this.total = datos.total;
            this.pedidoDetalle = datos.pedidoDetalle;
            return;
        }
        this.idPedido = this.idPedido;
        this.idCliente = this.idCliente;
        this.nombreCliente = this.nombreCliente;
        this.total = this.total;
        this.pedidoDetalle = new Array<PedidoDetalle>();
    }

    agregarProducto(producto: Productos){
        let pedidoDetalle: PedidoDetalle = new PedidoDetalle();
        pedidoDetalle.cantidad = 1;
        pedidoDetalle.nombreProducto = producto.nombre;
        pedidoDetalle.precio = producto.precio;
        pedidoDetalle.idProducto = producto.id;
        pedidoDetalle.total = pedidoDetalle.cantidad * pedidoDetalle.precio;
    
        let existe: number = this.pedidoDetalle.filter( x => x.idProducto == producto.id).length;
        if( existe > 0){
            let posicion: number = this.pedidoDetalle.findIndex(x => x.idProducto == producto.id);
            this.pedidoDetalle[posicion].cantidad++;
            this.pedidoDetalle[posicion].total = this.pedidoDetalle[posicion].cantidad * this.pedidoDetalle[posicion].precio;
        }
        else{
            this.pedidoDetalle.push(pedidoDetalle);
        }


        this.actualizarTotal();
    }

    private actualizarTotal(){
        this.total = 0;
        for(let produto of this.pedidoDetalle){
            this.total = this.total + produto.total;
        }

        /*
        this.pedidoDetalle.forEach(producto => {
            this.total = this.total + produto.total;
        });
        */
    }

    actualizarCantidades(posicion: number){
        this.pedidoDetalle[posicion].total = this.pedidoDetalle[posicion].cantidad * this.pedidoDetalle[posicion].precio;
        this.actualizarTotal();
    }
}