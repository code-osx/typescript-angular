import { Component, OnInit } from '@angular/core';

interface Clientes{
  nombre: string;
  apellidos: string;
  edad: number;
}
interface Productos{
  nombre: string;
  precio: number;
}

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.scss']
})
export class ClientesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  guardarClientes(){
    let clientes: Array<Clientes> = new Array<Clientes>();
    clientes.push({
      nombre: 'Oscar',
      apellidos: 'Sanchez',
      edad: 23
    },
    {
      nombre: 'Ariadna',
      apellidos: 'Sanchez',
      edad: 18
    });
    localStorage.setItem("clientes", JSON.stringify(clientes));
  }
  guardarProductos(){
    let productos: Array<Productos> = new Array<Productos>();
    productos.push({
      nombre: 'Laptop',
      precio: 23000
    },
    {
      nombre: 'PC GAMER',
      precio: 43000
    });
    localStorage.setItem("productos", JSON.stringify(productos));
  }

  eliminarClientes(){
    localStorage.removeItem("clientes");
  }

  eliminarProductos(){
    localStorage.removeItem("productos");
  }

  eliminarTodo(){
    localStorage.clear();
  }

  get clientesLocales(): Clientes[]{
    let clientesLocalStorage: Clientes[] = JSON.parse(localStorage.getItem("clientes"));
    if(clientesLocalStorage == null){
      return new Array<Clientes>();
    }
    return clientesLocalStorage;
  }

  get productosLocales(): Productos[]{
    let productosLocalStorage: Productos[] = JSON.parse(localStorage.getItem("productos"));
    if(productosLocalStorage == null){
      return new Array<Productos>();
    }
    return productosLocalStorage;
  }
}
