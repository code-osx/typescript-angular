import { Component, OnInit } from '@angular/core';
import { Productos } from '../models/productos';
import { PedidosService } from '../services/pedidos.service';
import { ProductoService } from '../services/producto.service';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.scss']
})
export class ProductosComponent implements OnInit {
  productos: Array<Productos> =  new Array<Productos>();

  constructor( public productoServicio: ProductoService, public pedidosServicio: PedidosService) { }

  ngOnInit(): void {
    this.productos = this.productoServicio.productosLocalStorage;
  }

  buscarProducto(event){
    let nombreBuscar: string = event.target.value;

    this.productos = this.productoServicio.productosLocalStorage.filter( cliente => {
      return cliente.nombre.toLocaleLowerCase().includes(nombreBuscar.toLowerCase());
    });
  }

  agregar(producto: Productos){
    this.pedidosServicio.pedido.agregarProducto(producto);
    this.pedidosServicio.guardarLocalStorage();
    alert("Producto agregado");
  }
}
