import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Clientes } from '../models/clientes';
import { ClienteService } from '../services/cliente.service';
import { PedidosService } from '../services/pedidos.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  clientes: Array<Clientes> =  new Array<Clientes>();
  constructor(public clientesServicio: ClienteService, public pedidosServicio: PedidosService, private route: Router) { }

  ngOnInit(): void {
    this.clientes = this.clientesServicio.clientesLocalStorage;
  }

  buscarCliente(event){
    let nombreBuscar: string = event.target.value;

    this.clientes = this.clientesServicio.clientesLocalStorage.filter( cliente => {
      return cliente.nombre.toLocaleLowerCase().includes(nombreBuscar.toLowerCase());
    });
  }

  irAProductos(cliente: Clientes){
    this.pedidosServicio.pedido.idCliente = cliente.id;
    this.pedidosServicio.pedido.nombreCliente = cliente.nombre+' '+cliente.apellidos;
    this.pedidosServicio.guardarLocalStorage();
    this.route.navigateByUrl("/productos");
  }
}
