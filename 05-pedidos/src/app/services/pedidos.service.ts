import { Injectable } from '@angular/core';
import { PedidoDetalle } from '../models/pedido-detalle';
import { Pedidos } from '../models/pedidos';
import { Productos } from '../models/productos';

@Injectable({
  providedIn: 'root'
})
export class PedidosService {
  pedido: Pedidos = new Pedidos();

  constructor( ) {
    this.pedido = this.ultimoPedido;
  }

  guardarLocalStorage(){
    localStorage.setItem("ultimoPedido", JSON.stringify(this.pedido));
  }

  get ultimoPedido(): Pedidos{
    let pedidoDeLS: Pedidos = new Pedidos( JSON.parse(localStorage.getItem("ultimoPedido")) );

    if(pedidoDeLS == null){
      return new Pedidos();
    }

    return pedidoDeLS;
  }

  guardarPedido(){
    let listadoPedidos: Pedidos[] = new Array<Pedidos>();
    listadoPedidos = this.listadoPedidosDeLS;
    this.pedido.idPedido = listadoPedidos.length+1;
    listadoPedidos.push(this.pedido);
    localStorage.setItem("pedidos", JSON.stringify(listadoPedidos));

    localStorage.removeItem("ultimoPedido");
    this.pedido = new Pedidos(null);
  }

  get listadoPedidosDeLS(): Pedidos[]{
    let pedidos: Pedidos[] = JSON.parse( localStorage.getItem("pedidos"));
    if(pedidos == null){
      return new Array<Pedidos>();
    }
    return pedidos.sort((a,b) => b.idPedido - a.idPedido);
  }
}
