import { Injectable } from '@angular/core';
import { Clientes } from '../models/clientes';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  constructor() { }

  agregarLocalStorage(cliente: Clientes){
    let clientesAntiguos: Clientes[] = this.clientesLocalStorage;
    cliente.id = clientesAntiguos.length+1;
    clientesAntiguos.push(cliente);

    localStorage.setItem("clientes", JSON.stringify(clientesAntiguos));
  }

  get clientesLocalStorage(): Clientes[]{
    let clientesDeLS: Clientes[] = JSON.parse( localStorage.getItem("clientes") );
    
    if(clientesDeLS == null){
      return new Array<Clientes>();
    }

    return clientesDeLS;
  }
}
