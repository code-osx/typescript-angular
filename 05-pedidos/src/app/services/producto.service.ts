import { Injectable } from '@angular/core';
import { Productos } from '../models/Productos';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  constructor() { }

  agregarLocalStorage(producto: Productos){
    let productosAntiguos: Productos[] = this.productosLocalStorage;
    producto.id = productosAntiguos.length+1;
    productosAntiguos.push(producto);

    localStorage.setItem("Productos", JSON.stringify(productosAntiguos));
  }

  get productosLocalStorage(): Productos[]{
    let productosDeLS: Productos[] = JSON.parse( localStorage.getItem("Productos") );
    
    if(productosDeLS == null){
      return new Array<Productos>();
    }

    return productosDeLS;
  }
}
