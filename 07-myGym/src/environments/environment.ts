// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyB6Ecx1R8B1J5Dn2GVIlfh_IuBfOJnoAQY",
    authDomain: "mygym-aa218.firebaseapp.com",
    databaseURL: "https://mygym-aa218.firebaseio.com",
    projectId: "mygym-aa218",
    storageBucket: "mygym-aa218.appspot.com",
    messagingSenderId: "191474182480",
    appId: "1:191474182480:web:be3da937cc78dc3d1ae455",
    measurementId: "G-NPRDJX592K"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
