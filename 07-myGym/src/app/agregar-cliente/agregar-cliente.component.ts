import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MensajesService } from '../services/mensajes.service';

@Component({
  selector: 'app-agregar-cliente',
  templateUrl: './agregar-cliente.component.html',
  styleUrls: ['./agregar-cliente.component.scss']
})
export class AgregarClienteComponent implements OnInit {

  clienteForm: FormGroup;
  porcentajeUpload: number = 0;
  imgUrl: string = '';

  constructor(private formBuilder: FormBuilder,
              private storage: AngularFireStorage, 
              private db: AngularFirestore,
              private msj: MensajesService) 
              { }

  ngOnInit(): void {
    this.clienteForm = this.formBuilder.group({
      nombre: ['', Validators.required],
      apellidos: ['', Validators.required],
      email: ['', Validators.compose([
        Validators.required,
        Validators.email
      ])],
      ine: [''],
      fechaNacimiento: ['', Validators.required],
      telefono: ['', Validators.required],
      imgUrl: ['', Validators.required]
    });
  }

  agregar(){
    this.clienteForm.value.imgUrl = this.imgUrl;
    this.clienteForm.value.fechaNacimiento = new Date(this.clienteForm.value.fechaNacimiento);
    this.db.collection('clientes').add(this.clienteForm.value).then((termino) => {
      this.msj.mensajeCorrecto('Agregado','El cliente se agregó correctamente');
      this.clienteForm.reset();
      console.log('Registro creado');
    });
  }

  subirImagen(event){
    //console.log(event);
    if(event.target.files.length > 0){
      let file = event.target.files[0];
      let nombre = new Date().getTime().toString();
      let mimeType = file.name.slice(file.name.toString().lastIndexOf('.'));

      let ruta = 'clientes/'+nombre+mimeType;
      const referencia = this.storage.ref(ruta);
      const task = referencia.put(file);

      task.percentageChanges().subscribe((porcentaje) => {
        this.porcentajeUpload = parseInt(porcentaje.toString());
      });

      task.then((obj) => {
        //console.log("La imagen fue subida");
        referencia.getDownloadURL().subscribe((url) => {
          this.imgUrl = url;
        });
      });
    }
    
  }
}
