import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { MensajesService } from '../services/mensajes.service';

@Component({
  selector: 'app-editar-cliente',
  templateUrl: './editar-cliente.component.html',
  styleUrls: ['./editar-cliente.component.scss']
})
export class EditarClienteComponent implements OnInit {

  clienteForm: FormGroup;
  porcentajeUpload: number = 0;
  imgUrl: string = '';
  id: string;

  constructor(private formBuilder: FormBuilder, 
              private storage: AngularFireStorage,
              private db: AngularFirestore,
              private activeRoute: ActivatedRoute,
              private msj: MensajesService)
              { }

  ngOnInit(): void {

    this.clienteForm = this.formBuilder.group({
      nombre: ['', Validators.required],
      apellidos: ['', Validators.required],
      email: ['', Validators.compose([
        Validators.required,
        Validators.email
      ])],
      ine: [''],
      fechaNacimiento: ['', Validators.required],
      telefono: ['', Validators.required],
      imgUrl: ['']
    });

    this.id = this.activeRoute.snapshot.params.clienteId;
    this.db.doc<any>('clientes'+'/'+this.id).valueChanges().subscribe((cliente) => {
      this.clienteForm.setValue({
        nombre: cliente.nombre,
        apellidos: cliente.apellidos,
        email: cliente.email,
        ine: cliente.ine,
        fechaNacimiento: new Date(cliente.fechaNacimiento.seconds * 1000).toISOString().slice(0, 10),
        telefono: cliente.telefono,
        imgUrl: ''
      });
      this.imgUrl = cliente.imgUrl;
    });

  }

  editar(){
    this.clienteForm.value.imgUrl = this.imgUrl;
    this.clienteForm.value.fechaNacimiento = new Date(this.clienteForm.value.fechaNacimiento);
    this.db.doc('clientes/'+this.id).update(this.clienteForm.value).then(() => {
      this.msj.mensajeCorrecto('Editado','La información del cliente fue actualizada correctamente');
    }).catch(() => {
      this.msj.mensajeError('Error','Ocurrio un error al actualizar la información del cliente');
    });
  }

  subirImagen(event){
    //console.log(event);
    if(event.target.files.length > 0){
      let file = event.target.files[0];
      let nombre = new Date().getTime().toString();
      let mimeType = file.name.slice(file.name.toString().lastIndexOf('.'));

      let ruta = 'clientes/'+nombre+mimeType;
      const referencia = this.storage.ref(ruta);
      const task = referencia.put(file);

      task.percentageChanges().subscribe((porcentaje) => {
        this.porcentajeUpload = parseInt(porcentaje.toString());
      });

      task.then((obj) => {
        //console.log("La imagen fue subida");
        referencia.getDownloadURL().subscribe((url) => {
          this.imgUrl = url;
        });
      });
    }
    
  }

}
