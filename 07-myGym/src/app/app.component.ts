import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import firebase from 'firebase/app';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'myGym';
  usuario: firebase.User;
  cargando: boolean = true;

  constructor(private auth: AngularFireAuth){
    this.auth.user.subscribe((user) => {
      this.cargando = false;
      this.usuario = user;
    })
  }
  
}
