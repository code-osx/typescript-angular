import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  datosCorrectos:boolean = true;
  errorText: string;

  constructor(private formBuilder: FormBuilder, private auth: AngularFireAuth, private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.compose([
        Validators.required,
        Validators.email
      ])],
      pass: ['', Validators.required]
    });
  }

  login() {
    if(this.loginForm.valid){
      this.datosCorrectos = true;
      this.spinner.show();
      this.auth.signInWithEmailAndPassword(this.loginForm.value.email, this.loginForm.value.pass).then((user) => {
        console.log(user);
        this.spinner.hide();
      }).catch((error) => {
        this.spinner.hide();
        this.datosCorrectos = false;
        this.errorText = "El correo o la contraseña no son correctos";
      });
    }
    else{
      this.datosCorrectos = false;
      this.errorText = "Asegurate de haber llenado correctamente tus datos";
    }
  }

}
