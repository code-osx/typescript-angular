import { DocumentReference } from '@angular/fire/firestore';

export class Cliente{
    id: string;
    apellidos: string;
    email: string;
    fechaNacimiento: Date;
    imgUrl: string;
    ine: string;
    nombre: string;
    telefono: number;
    ref: DocumentReference;
    visible: boolean;
}