import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Cliente } from '../models/cliente';

@Component({
  selector: 'app-sel-cliente',
  templateUrl: './sel-cliente.component.html',
  styleUrls: ['./sel-cliente.component.scss']
})
export class SelClienteComponent implements OnInit {

  clientes: Cliente[] = new Array<Cliente>();
  @Input('nombre') nombre: string;
  @Output('seleccionoCliente') seleccionoCliente = new EventEmitter();
  @Output('canceloCliente') canceloCliente = new EventEmitter();
  
  constructor( private db: AngularFirestore) { }

  ngOnInit(): void {
    this.db.collection<Cliente>('clientes').get().subscribe((resultado) => {
      this.clientes.length = 0;

      resultado.docs.forEach((item) => {
        let cliente: any = item.data();
        cliente.id = item.id;
        cliente.ref = item.ref;
        cliente.visible = false;

        this.clientes.push(cliente);
      });
    });
  }

  buscarCliente(nombre: string){
    this.clientes.forEach((cliente) => {
      if(cliente.nombre.toLowerCase().includes(nombre.toLowerCase())){
        cliente.visible = true;
      }
      else{
        cliente.visible = false;
      }
    })
  }

  selCliente(cliente: Cliente){
    this.nombre = cliente.nombre+' '+cliente.apellidos;
    this.clientes.forEach((cliente) => {
      cliente.visible = false;
    });

    this.seleccionoCliente.emit(cliente);
  }

  cancelarCliente(){
    this.nombre = undefined;
    this.canceloCliente.emit();
  }
}
