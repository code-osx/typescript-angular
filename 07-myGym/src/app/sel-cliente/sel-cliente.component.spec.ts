import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelClienteComponent } from './sel-cliente.component';

describe('SelClienteComponent', () => {
  let component: SelClienteComponent;
  let fixture: ComponentFixture<SelClienteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelClienteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelClienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
