import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Precio } from '../models/precio';
import { MensajesService } from '../services/mensajes.service';

@Component({
  selector: 'app-precios',
  templateUrl: './precios.component.html',
  styleUrls: ['./precios.component.scss']
})
export class PreciosComponent implements OnInit {

  preciosForm: FormGroup;
  precios: Precio[] = new Array<Precio>();
  esEditar: boolean = false;
  id: string = '';
  
  constructor(private formBuilder: FormBuilder, private db: AngularFirestore, private msj: MensajesService) { }

  ngOnInit(): void {
    this.preciosForm = this.formBuilder.group({
      nombre: ['', Validators.required],
      costo: ['', Validators.required],
      duracion: ['', Validators.required],
      tipoDuracion: ['', Validators.required],
    });

    this.mostrarPrecios();
  }

  mostrarPrecios(){
    this.precios.length = 0;

    this.db.collection<Precio>('precios').get().subscribe((resultado) => {
      resultado.docs.forEach((dato) => {
        let precio = dato.data() as Precio;
        precio.id = dato.id;
        precio.ref = dato.ref;

        this.precios.push(precio);
      });
    });
  }
  
  agregar(){
    this.db.collection('precios').add(this.preciosForm.value).then(() => {
      this.msj.mensajeCorrecto('Precio agregado','El precio se ha agregado correctamente');
      this.preciosForm.reset();
      this.mostrarPrecios();
    }).catch(() => {
      this.msj.mensajeError('Error','Ha ocurrido un error');
    });
  }

  editar( precio: Precio ){
    this. esEditar = true;
    
    this.preciosForm.setValue({
      nombre: precio.nombre,
      costo: precio.costo,
      duracion: precio.duracion,
      tipoDuracion: precio.tipoDuracion
    });

    this.id = precio.id;
  }

  guardarCambios(){
    this.db.doc('precios/'+this.id).update(this.preciosForm.value).then(() => {
      this.msj.mensajeCorrecto('Editado','La información del precio ha sido actualizada');
      this.preciosForm.reset();
      this. esEditar = false;
      this.mostrarPrecios();
    }).catch(() => {
      this.msj.mensajeError('Error','Ha ocurrido un error');
    });
  }

}
